# Parser de trame NMEA

Les bateaux au travers de leur transpondeur communiquent en permanence des informations que ce soit aux ports ou aux aux autres bateaux.

Ces messages sont envoyé au moyen d'ondes hertziennes de 161.975 MHz et 162.025 MHz.

Les messages binaires sont encodé en ASCII compressé et transmis sous la forme de phrases.

Le but de ce projet est de parser les trames et d'en retirer tous les informations intéressantes.

Pour cela je me suis appuyé sur un document de rétro-engineering. https://gpsd.gitlab.io/gpsd/AIVDM.htm

Il existe une grande variété de messages. 

Pour le moment je me suis concentré sur les messages de type 1 et 5.


## Messages 

### Structure d'une trame

```
!AIVDM,1,1,,B,B3I0CLP052Ms0>vKqo2ikws5wP06,0*5C
```

Tout la partie 

```
!AIVDM,1,1,,B,
```

constitue le header du message il renseigne sur tout un tas d'informations détaillée [ici](https://gpsd.gitlab.io/gpsd/AIVDM.html#_aivdmaivdo_sentence_layer).

Le message possède aussi un checksum: https://rietman.wordpress.com/2008/09/25/how-to-calculate-the-nmea-checksum/

```
,0*5C
```

Le message en lui-même est 

```
177KQJ5000G?tO`K>RA1wUbN0TKH
```
Il est compressé et doit être transformé en une suite de 0 et 1 pour être traité:

```
010010000011011001000000010011011100100000000000000101000010011101111011000000001110111110011011111001110111000010110001110011111111111011000101111111100000000000000110
```

Ceci est fait par cette table https://gpsd.gitlab.io/gpsd/AIVDM.html#_aivdmaivdo_payload_armoring

Un fois ceci fais le message peut-être parsé.

#### Fragmentation des messages

Si le header a 

```
!AIVDM,2,1,3,B
```
Cela signifie qu'une trame

```
!AIVDM,2,2,3,B,
```

Est nécessaire pour avoir le message complet.

Certaines trames doivent donc être bufferisées.

https://gpsd.gitlab.io/gpsd/AIVDM.html#_aivdmaivdo_sentence_layer

_Attention_ : L'ordre des trames n'est pas déterministes, une trame peut arriver entre les 2 fragments, mais n'aura pas le même ID.

### Message de type 1 : Position de classe A

Ce sont les messages qui contiennent les informations de positions et d'allures du bateau.

Ces messages ne sont pas fragmentés.

https://gpsd.gitlab.io/gpsd/AIVDM.html#_types_1_2_and_3_position_report_class_a

### Message de type 5 : Informations de destination et statiques

Ces messages donnent des détails sur la structure du bateau ainsi que sa destination.

Ces messages sont fragmentés.

https://gpsd.gitlab.io/gpsd/AIVDM.html#_type_5_static_and_voyage_related_data

## Utiliser le système

Pour le moment tout est en WIP.

Il existe un point d'entré de l'application ici:

https://gitlab.com/Akanoa/ais_parser/-/tree/master/trust_waves/src

Pour simuler des flux, je vous conseille NMEA simulator : https://panaaj.bitbucket.io/

Le système écoute sur le port 3038/tcp.

https://gitlab.com/Akanoa/ais_parser/-/blob/master/trust_waves/src/adapters/tcp.rs#L71

Vous devriez avoir qqch comme:

```
PositionReportClassA(MessageType1 { message_type: 1, repeat: 0, maritime_mobile_service_identity: 503999999, navigation_status: UnderWayUsingEngine, turning_rate: NotAvailable, speed_over_ground: Value(4.8), position_accuracy: true, longitude: East(138.50035), latitude: South(34.998974), course_over_ground: Some(16.5), heading: Some(16), second_of_timestamp: Value(52), maneuver_indicator: NotAvailable, receiver_autonomous_integrity_monitoring: false, radio_status: 0 })

StaticAndVoyage(MessageType5 { message_type: 5, repeat: 0, maritime_mobile_service_identity: 503999999, ais_version: 0, imo: 0, call_sign: "SIM1234", ship_name: "NMEASIM", ship_type: PleasureCraft, to_bow: 10, to_stern: 10, to_port: 5, to_starboard: 5, epfd: Undefined, month: Value(3), hour: Value(6), day: Value(1), minute: Value(46), draught: 1.0, destination: "SYDNEY", dte: Some(false) })
```
