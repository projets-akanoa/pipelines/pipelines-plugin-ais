use crate::parsers::string::parse_as_string;
use crate::parsers::utils::parsers::complete::{
    parser_bool_field, parser_u16_field, parser_u32_field, parser_u8_field,
};
use nom::bytes::complete::take;
use nom::combinator::{map, opt};
use nom::sequence::tuple;
use nom::IResult;
use serde::{Deserialize, Serialize};
use std::fmt::{Display, Formatter};

#[derive(Debug, Eq, PartialEq, Clone, Serialize, Deserialize)]
enum ShipType {
    NotAvailable,
    Wig,
    WigHazardousA,
    WigHazardousB,
    WigHazardousC,
    WigHazardousD,
    Fishing,
    Towing,
    TowingBig,
    Dredging,
    Driving,
    Military,
    Sailing,
    PleasureCraft,
    Hsc,
    HscHazardousA,
    HscHazardousB,
    HscHazardousC,
    HscHazardousD,
    HscNoInformation,
    PilotVessel,
    SearchAndRescue,
    Tug,
    PortTender,
    AntiPollution,
    LawEnforcement,
    LocalVessel,
    MedicalTransport,
    NonCombatant,
    Passenger,
    PassengerHazardousA,
    PassengerHazardousB,
    PassengerHazardousC,
    PassengerHazardousD,
    PassengerNoInformation,
    Cargo,
    CargoHazardousA,
    CargoHazardousB,
    CargoHazardousC,
    CargoHazardousD,
    CargoNoInformation,
    Tanker,
    TankerHazardousA,
    TankerHazardousB,
    TankerHazardousC,
    TankerHazardousD,
    TankerNoInformation,
    Other,
    OtherHazardousA,
    OtherHazardousB,
    OtherHazardousC,
    OtherHazardousD,
    OtherNoInformation,
    Undefined,
}

impl ShipType {
    fn from_u8(ship_type: u8) -> ShipType {
        match ship_type {
            x if (x > 99) | (x == 0) => ShipType::NotAvailable,
            20 => ShipType::Wig,
            21 => ShipType::WigHazardousA,
            22 => ShipType::WigHazardousB,
            23 => ShipType::WigHazardousC,
            24 => ShipType::WigHazardousD,
            30 => ShipType::Fishing,
            31 => ShipType::Towing,
            32 => ShipType::TowingBig,
            33 => ShipType::Dredging,
            34 => ShipType::Driving,
            35 => ShipType::Military,
            36 => ShipType::Sailing,
            37 => ShipType::PleasureCraft,
            40 => ShipType::Hsc,
            41 => ShipType::HscHazardousA,
            42 => ShipType::HscHazardousB,
            43 => ShipType::HscHazardousC,
            44 => ShipType::HscHazardousD,
            49 => ShipType::HscNoInformation,
            50 => ShipType::PilotVessel,
            51 => ShipType::SearchAndRescue,
            52 => ShipType::Tug,
            53 => ShipType::PortTender,
            54 => ShipType::AntiPollution,
            55 => ShipType::LawEnforcement,
            56 | 57 => ShipType::LocalVessel,
            58 => ShipType::MedicalTransport,
            59 => ShipType::NonCombatant,
            60 => ShipType::Passenger,
            61 => ShipType::PassengerHazardousA,
            62 => ShipType::PassengerHazardousB,
            63 => ShipType::PassengerHazardousC,
            64 => ShipType::PassengerHazardousD,
            69 => ShipType::PassengerNoInformation,
            70 => ShipType::Cargo,
            71 => ShipType::CargoHazardousA,
            72 => ShipType::CargoHazardousB,
            73 => ShipType::CargoHazardousC,
            74 => ShipType::CargoHazardousD,
            79 => ShipType::CargoNoInformation,
            80 => ShipType::Tanker,
            81 => ShipType::TankerHazardousA,
            82 => ShipType::TankerHazardousB,
            83 => ShipType::TankerHazardousC,
            84 => ShipType::TankerHazardousD,
            89 => ShipType::TankerNoInformation,
            90 => ShipType::Other,
            91 => ShipType::OtherHazardousA,
            92 => ShipType::OtherHazardousB,
            93 => ShipType::OtherHazardousC,
            94 => ShipType::OtherHazardousD,
            99 => ShipType::OtherNoInformation,
            _ => ShipType::Undefined,
        }
    }
}

#[derive(Debug, Eq, PartialEq, Clone, Serialize, Deserialize)]
#[allow(clippy::upper_case_acronyms)]
enum EPFD {
    Undefined,
    GPS,
    GLONASS,
    Combined,
    LoranC,
    Chayka,
    Integrated,
    Surveyed,
    Galileo,
}

impl EPFD {
    fn from_u8(value: u8) -> EPFD {
        match value {
            1 => EPFD::GPS,
            2 => EPFD::GLONASS,
            3 => EPFD::Combined,
            4 => EPFD::LoranC,
            5 => EPFD::Chayka,
            6 => EPFD::Integrated,
            7 => EPFD::Surveyed,
            8 => EPFD::Galileo,
            _ => EPFD::Undefined,
        }
    }
}

#[derive(Debug, Eq, PartialEq, Clone, Serialize, Deserialize)]
enum Month {
    Value(u8),
    Undefined,
}

impl Month {
    fn from_u8(value: u8) -> Month {
        match value {
            x if (1..=12).contains(&x) => Month::Value(x),
            _ => Month::Undefined,
        }
    }
}

#[derive(Debug, Eq, PartialEq, Clone, Serialize, Deserialize)]
enum Day {
    Value(u8),
    Undefined,
}

impl Day {
    fn from_u8(value: u8) -> Day {
        match value {
            x if (1..=31).contains(&x) => Day::Value(x),
            _ => Day::Undefined,
        }
    }
}

#[derive(Debug, Eq, PartialEq, Clone, Serialize, Deserialize)]
enum Hour {
    Value(u8),
    Undefined,
}

impl Hour {
    fn from_u8(value: u8) -> Hour {
        match value {
            x if x <= 23 => Hour::Value(x),
            _ => Hour::Undefined,
        }
    }
}

#[derive(Debug, Eq, PartialEq, Clone, Serialize, Deserialize)]
enum Minute {
    Value(u8),
    Undefined,
}

impl Minute {
    fn from_u8(value: u8) -> Minute {
        match value {
            x if x <= 59 => Minute::Value(x),
            _ => Minute::Undefined,
        }
    }
}

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize)]
pub struct MessageType5 {
    message_type: u8,
    repeat: u8,
    pub maritime_mobile_service_identity: u32,
    ais_version: u8,
    imo: u32,
    call_sign: String,
    pub ship_name: String,
    ship_type: ShipType,
    pub to_bow: u16,      // proue ( l'avant)
    pub to_stern: u16,    // poupe (l'arrière)
    pub to_port: u8,      // bâbord (à gauche de la proue)
    pub to_starboard: u8, // tribord (à droite de la proue),
    epfd: EPFD,
    month: Month,
    hour: Hour,
    day: Day,
    minute: Minute,
    draught: f32,
    destination: String,
    dte: Option<bool>,
}

impl Display for MessageType5 {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

fn parse_ship_type(input: &str) -> IResult<&str, ShipType> {
    map(parser_u8_field(8), ShipType::from_u8)(input)
}

fn parse_epfd(input: &str) -> IResult<&str, EPFD> {
    map(parser_u8_field(4), EPFD::from_u8)(input)
}

fn parse_draught(input: &str) -> IResult<&str, f32> {
    map(parser_u8_field(8), |value| (value as f32) / 10f32)(input)
}

fn parse_month(input: &str) -> IResult<&str, Month> {
    map(parser_u8_field(4usize), Month::from_u8)(input)
}

fn parse_day(input: &str) -> IResult<&str, Day> {
    map(parser_u8_field(5usize), Day::from_u8)(input)
}

fn parse_hour(input: &str) -> IResult<&str, Hour> {
    map(parser_u8_field(5usize), Hour::from_u8)(input)
}

fn parse_minute(input: &str) -> IResult<&str, Minute> {
    map(parser_u8_field(6usize), Minute::from_u8)(input)
}

pub fn parser_payload_type5(payload: &str) -> IResult<&str, MessageType5> {
    let (
        input,
        (
            message_type,
            repeat,
            maritime_mobile_service_identity,
            ais_version,
            imo,
            call_sign,
            ship_name,
            ship_type,
            to_bow,
            to_stern,
            to_port,
            to_starboard,
            epfd,
            month,
            day,
            hour,
            minute,
            draught,
            destination,
            dte,
            _,
        ),
    ) = tuple((
        parser_u8_field(6usize),
        parser_u8_field(2usize),
        parser_u32_field(30usize),
        parser_u8_field(2usize),
        parser_u32_field(30usize),
        parse_as_string(42usize),
        parse_as_string(120usize),
        parse_ship_type,
        parser_u16_field(9usize),
        parser_u16_field(9usize),
        parser_u8_field(6usize),
        parser_u8_field(6usize),
        parse_epfd,
        parse_month,
        parse_day,
        parse_hour,
        parse_minute,
        parse_draught,
        parse_as_string(120usize),
        opt(parser_bool_field),
        opt(take(1usize)),
    ))(payload)?;

    Ok((
        input,
        MessageType5 {
            message_type,
            repeat,
            maritime_mobile_service_identity,
            ais_version,
            imo,
            call_sign,
            ship_name,
            ship_type,
            to_bow,
            to_stern,
            to_port,
            to_starboard,
            epfd,
            month,
            day,
            hour,
            minute,
            draught,
            destination,
            dte,
        },
    ))
}

#[cfg(test)]
mod tests {
    use crate::parsers::message::type5::{
        parse_day, parse_draught, parse_epfd, parse_hour, parse_minute, parse_month,
        parse_ship_type, parser_payload_type5, Day, Hour, MessageType5, Minute, Month, ShipType,
        EPFD,
    };
    use crate::parsers::utils::data::uncompress_payload;

    #[test]
    fn should_parse_ship_type() {
        assert_eq!(
            parse_ship_type("00000000aaaa"),
            Ok(("aaaa", ShipType::NotAvailable))
        ); // 0
        assert_eq!(parse_ship_type("00010100aaaa"), Ok(("aaaa", ShipType::Wig))); // 20
        assert_eq!(
            parse_ship_type("00010101aaaa"),
            Ok(("aaaa", ShipType::WigHazardousA))
        ); // 21
        assert_eq!(
            parse_ship_type("00010110aaaa"),
            Ok(("aaaa", ShipType::WigHazardousB))
        ); // 22
        assert_eq!(
            parse_ship_type("00010111aaaa"),
            Ok(("aaaa", ShipType::WigHazardousC))
        ); // 23
        assert_eq!(
            parse_ship_type("00011000aaaa"),
            Ok(("aaaa", ShipType::WigHazardousD))
        ); // 23
        assert_eq!(
            parse_ship_type("00011110aaaa"),
            Ok(("aaaa", ShipType::Fishing))
        ); // 30
        assert_eq!(
            parse_ship_type("00011111aaaa"),
            Ok(("aaaa", ShipType::Towing))
        ); // 31
        assert_eq!(
            parse_ship_type("00100000aaaa"),
            Ok(("aaaa", ShipType::TowingBig))
        ); // 32
        assert_eq!(
            parse_ship_type("00100001aaaa"),
            Ok(("aaaa", ShipType::Dredging))
        ); // 33
        assert_eq!(
            parse_ship_type("00100010aaaa"),
            Ok(("aaaa", ShipType::Driving))
        ); // 34
        assert_eq!(
            parse_ship_type("00100011aaaa"),
            Ok(("aaaa", ShipType::Military))
        ); // 35
        assert_eq!(
            parse_ship_type("00100100aaaa"),
            Ok(("aaaa", ShipType::Sailing))
        ); // 36
        assert_eq!(
            parse_ship_type("00100101aaaa"),
            Ok(("aaaa", ShipType::PleasureCraft))
        ); // 37
        assert_eq!(parse_ship_type("00101000aaaa"), Ok(("aaaa", ShipType::Hsc))); // 40
        assert_eq!(
            parse_ship_type("00101001aaaa"),
            Ok(("aaaa", ShipType::HscHazardousA))
        ); // 41
        assert_eq!(
            parse_ship_type("00101010aaaa"),
            Ok(("aaaa", ShipType::HscHazardousB))
        ); // 42
        assert_eq!(
            parse_ship_type("00101011aaaa"),
            Ok(("aaaa", ShipType::HscHazardousC))
        ); // 43
        assert_eq!(
            parse_ship_type("00101100aaaa"),
            Ok(("aaaa", ShipType::HscHazardousD))
        ); // 44
        assert_eq!(
            parse_ship_type("00110001aaaa"),
            Ok(("aaaa", ShipType::HscNoInformation))
        ); // 49
        assert_eq!(
            parse_ship_type("00110010aaaa"),
            Ok(("aaaa", ShipType::PilotVessel))
        ); // 50
        assert_eq!(
            parse_ship_type("00110011aaaa"),
            Ok(("aaaa", ShipType::SearchAndRescue))
        ); // 51
        assert_eq!(parse_ship_type("00110100aaaa"), Ok(("aaaa", ShipType::Tug))); // 52
        assert_eq!(
            parse_ship_type("00110101aaaa"),
            Ok(("aaaa", ShipType::PortTender))
        ); // 53
        assert_eq!(
            parse_ship_type("00110110aaaa"),
            Ok(("aaaa", ShipType::AntiPollution))
        ); // 54
        assert_eq!(
            parse_ship_type("00110111aaaa"),
            Ok(("aaaa", ShipType::LawEnforcement))
        ); // 55
        assert_eq!(
            parse_ship_type("00110111aaaa"),
            Ok(("aaaa", ShipType::LawEnforcement))
        ); // 55
        assert_eq!(
            parse_ship_type("00111000aaaa"),
            Ok(("aaaa", ShipType::LocalVessel))
        ); // 56
        assert_eq!(
            parse_ship_type("00111001aaaa"),
            Ok(("aaaa", ShipType::LocalVessel))
        ); // 57
        assert_eq!(
            parse_ship_type("00111010aaaa"),
            Ok(("aaaa", ShipType::MedicalTransport))
        ); // 58
        assert_eq!(
            parse_ship_type("00111011aaaa"),
            Ok(("aaaa", ShipType::NonCombatant))
        ); // 59
        assert_eq!(
            parse_ship_type("00111100aaaa"),
            Ok(("aaaa", ShipType::Passenger))
        ); // 60
        assert_eq!(
            parse_ship_type("00111101aaaa"),
            Ok(("aaaa", ShipType::PassengerHazardousA))
        ); // 61
        assert_eq!(
            parse_ship_type("00111110aaaa"),
            Ok(("aaaa", ShipType::PassengerHazardousB))
        ); // 62
        assert_eq!(
            parse_ship_type("00111111aaaa"),
            Ok(("aaaa", ShipType::PassengerHazardousC))
        ); // 63
        assert_eq!(
            parse_ship_type("01000000aaaa"),
            Ok(("aaaa", ShipType::PassengerHazardousD))
        ); // 64
        assert_eq!(
            parse_ship_type("01000101aaaa"),
            Ok(("aaaa", ShipType::PassengerNoInformation))
        ); // 69
        assert_eq!(
            parse_ship_type("01000110aaaa"),
            Ok(("aaaa", ShipType::Cargo))
        ); // 70
        assert_eq!(
            parse_ship_type("01000111aaaa"),
            Ok(("aaaa", ShipType::CargoHazardousA))
        ); // 71
        assert_eq!(
            parse_ship_type("01001000aaaa"),
            Ok(("aaaa", ShipType::CargoHazardousB))
        ); // 72
        assert_eq!(
            parse_ship_type("01001001aaaa"),
            Ok(("aaaa", ShipType::CargoHazardousC))
        ); // 73
        assert_eq!(
            parse_ship_type("01001010aaaa"),
            Ok(("aaaa", ShipType::CargoHazardousD))
        ); // 74
        assert_eq!(
            parse_ship_type("01001111aaaa"),
            Ok(("aaaa", ShipType::CargoNoInformation))
        ); // 79
        assert_eq!(
            parse_ship_type("01010000aaaa"),
            Ok(("aaaa", ShipType::Tanker))
        ); // 80
        assert_eq!(
            parse_ship_type("01010001aaaa"),
            Ok(("aaaa", ShipType::TankerHazardousA))
        ); // 81
        assert_eq!(
            parse_ship_type("01010010aaaa"),
            Ok(("aaaa", ShipType::TankerHazardousB))
        ); // 82
        assert_eq!(
            parse_ship_type("01010011aaaa"),
            Ok(("aaaa", ShipType::TankerHazardousC))
        ); // 83
        assert_eq!(
            parse_ship_type("01010100aaaa"),
            Ok(("aaaa", ShipType::TankerHazardousD))
        ); // 84
        assert_eq!(
            parse_ship_type("01011001aaaa"),
            Ok(("aaaa", ShipType::TankerNoInformation))
        ); // 89
        assert_eq!(
            parse_ship_type("01011010aaaa"),
            Ok(("aaaa", ShipType::Other))
        ); // 90
        assert_eq!(
            parse_ship_type("01011011aaaa"),
            Ok(("aaaa", ShipType::OtherHazardousA))
        ); // 90
        assert_eq!(
            parse_ship_type("01011100aaaa"),
            Ok(("aaaa", ShipType::OtherHazardousB))
        ); // 91
        assert_eq!(
            parse_ship_type("01011101aaaa"),
            Ok(("aaaa", ShipType::OtherHazardousC))
        ); // 92
        assert_eq!(
            parse_ship_type("01011110aaaa"),
            Ok(("aaaa", ShipType::OtherHazardousD))
        ); // 93
        assert_eq!(
            parse_ship_type("01100011aaaa"),
            Ok(("aaaa", ShipType::OtherNoInformation))
        ); // 99

        for i in 1..19 {
            assert_eq!(
                parse_ship_type(&format!("{:08b}aaaa", i)),
                Ok(("aaaa", ShipType::Undefined))
            ); // 99
        }

        for i in vec![
            25, 26, 27, 28, 29, 38, 39, 45, 46, 47, 48, 65, 66, 67, 68, 75, 76, 77, 78, 85, 86, 87,
            88, 95, 96, 97, 98,
        ] {
            assert_eq!(
                parse_ship_type(&format!("{:08b}aaaa", i)),
                Ok(("aaaa", ShipType::Undefined))
            );
        }
    }

    #[test]
    fn should_parse_epfd() {
        assert_eq!(
            parse_epfd(&format!("{:04b}aaaa", 0)),
            Ok(("aaaa", EPFD::Undefined))
        );
        assert_eq!(
            parse_epfd(&format!("{:04b}aaaa", 1)),
            Ok(("aaaa", EPFD::GPS))
        );
        assert_eq!(
            parse_epfd(&format!("{:04b}aaaa", 2)),
            Ok(("aaaa", EPFD::GLONASS))
        );
        assert_eq!(
            parse_epfd(&format!("{:04b}aaaa", 3)),
            Ok(("aaaa", EPFD::Combined))
        );
        assert_eq!(
            parse_epfd(&format!("{:04b}aaaa", 4)),
            Ok(("aaaa", EPFD::LoranC))
        );
        assert_eq!(
            parse_epfd(&format!("{:04b}aaaa", 5)),
            Ok(("aaaa", EPFD::Chayka))
        );
        assert_eq!(
            parse_epfd(&format!("{:04b}aaaa", 6)),
            Ok(("aaaa", EPFD::Integrated))
        );
        assert_eq!(
            parse_epfd(&format!("{:04b}aaaa", 7)),
            Ok(("aaaa", EPFD::Surveyed))
        );
        assert_eq!(
            parse_epfd(&format!("{:04b}aaaa", 8)),
            Ok(("aaaa", EPFD::Galileo))
        );
        assert_eq!(
            parse_epfd(&format!("{:04b}aaaa", 9)),
            Ok(("aaaa", EPFD::Undefined))
        );
        assert_eq!(
            parse_epfd(&format!("{:04b}aaaa", 15)),
            Ok(("aaaa", EPFD::Undefined))
        );
    }

    #[test]
    fn should_parse_draught() {
        assert_eq!(
            parse_draught(&format!("{:08b}aaaa", 125)),
            Ok(("aaaa", 12.5f32))
        );
        assert_eq!(
            parse_draught(&format!("{:08b}aaaa", 12)),
            Ok(("aaaa", 1.2f32))
        );
        assert_eq!(parse_draught(&format!("{:08b}aaaa", 0)), Ok(("aaaa", 0f32)));
    }

    #[test]
    fn should_parse_month() {
        for i in 1..=12 {
            assert_eq!(
                parse_month(&format!("{:04b}aaaa", i)),
                Ok(("aaaa", Month::Value(i)))
            );
        }

        assert_eq!(
            parse_month(&format!("{:04b}aaaa", 0)),
            Ok(("aaaa", Month::Undefined))
        );
        assert_eq!(
            parse_month(&format!("{:04b}aaaa", 13)),
            Ok(("aaaa", Month::Undefined))
        );
    }

    #[test]
    fn should_parse_day() {
        for i in 1..=31 {
            assert_eq!(
                parse_day(&format!("{:05b}aaaa", i)),
                Ok(("aaaa", Day::Value(i)))
            );
        }

        assert_eq!(
            parse_day(&format!("{:05b}aaaa", 0)),
            Ok(("aaaa", Day::Undefined))
        );
    }

    #[test]
    fn should_parse_hour() {
        for i in 0..=23 {
            assert_eq!(
                parse_hour(&format!("{:05b}aaaa", i)),
                Ok(("aaaa", Hour::Value(i)))
            );
        }

        assert_eq!(
            parse_hour(&format!("{:05b}aaaa", 24)),
            Ok(("aaaa", Hour::Undefined))
        );
        assert_eq!(
            parse_hour(&format!("{:05b}aaaa", 28)),
            Ok(("aaaa", Hour::Undefined))
        );
    }

    #[test]
    fn should_parse_minute() {
        for i in 0..=59 {
            assert_eq!(
                parse_minute(&format!("{:06b}aaaa", i)),
                Ok(("aaaa", Minute::Value(i)))
            );
        }

        assert_eq!(
            parse_minute(&format!("{:06b}aaaa", 60)),
            Ok(("aaaa", Minute::Undefined))
        );
        assert_eq!(
            parse_minute(&format!("{:06b}aaaa", 62)),
            Ok(("aaaa", Minute::Undefined))
        );
    }

    #[test]
    fn should_parse_type5_message() {
        let payload = uncompress_payload(
            "54VE:802<@fL?HHsJ21<TiHE:1<P4@uN2222220t7B0;>C7<e?E25DTi0FH2Dk0CQ888881",
        )
        .unwrap();
        let result = parser_payload_type5(&payload[..424]);

        let expected = MessageType5 {
            message_type: 5,
            repeat: 0,
            maritime_mobile_service_identity: 308628000,
            ais_version: 0,
            imo: 9192167,
            call_sign: "C6FN6".to_string(),
            ship_name: "SILVER SHADOW".to_string(),
            ship_type: ShipType::Passenger,
            to_bow: 58,
            to_stern: 128,
            to_port: 11,
            to_starboard: 14,
            epfd: EPFD::LoranC,
            month: Month::Value(12),
            day: Day::Value(14),
            hour: Hour::Value(12),
            minute: Minute::Value(45),
            draught: 6.1,
            destination: "THURSDAY ISLAND".to_string(),
            dte: Some(false),
        };

        assert_eq!(result, Ok(("", expected)));
    }
}
