use crate::parsers::header::{Channel, Header, Protocol};
use crate::parsers::utils::data::{checksum, uncompress_payload};
use crate::parsers::utils::parsers::streaming::parse_fragment;
use nom::lib::std::cmp::Ordering;
use nom::lib::std::collections::HashMap;
use nom::lib::std::ops::Add;

#[derive(Debug, Eq, PartialEq)]
struct FragmentRawData {
    header: Header,
    payload: String,
}

impl From<FragmentRawData> for MessageData {
    fn from(fragment: FragmentRawData) -> Self {
        MessageData::new(fragment)
    }
}

impl Add for FragmentRawData {
    type Output = FragmentRawData;

    fn add(self, rhs: Self) -> Self::Output {
        FragmentRawData {
            header: rhs.header,
            payload: format!("{}{}", self.payload, rhs.payload),
        }
    }
}

impl Ord for FragmentRawData {
    fn cmp(&self, other: &Self) -> Ordering {
        self.header
            .current_fragment
            .cmp(&other.header.current_fragment)
    }
}

impl PartialOrd for FragmentRawData {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(
            self.header
                .current_fragment
                .cmp(&other.header.current_fragment),
        )
    }
}

#[derive(Debug, Eq, PartialEq)]
pub struct MessageHeader {
    protocol: Protocol,
    channel: Channel,
}

#[derive(Debug, Eq, PartialEq)]
pub struct MessageData {
    header: MessageHeader,
    pub payload: String,
}

impl MessageData {
    fn new(fragment: FragmentRawData) -> MessageData {
        MessageData {
            header: MessageHeader {
                protocol: fragment.header.protocol,
                channel: fragment.header.channel,
            },
            payload: fragment.payload,
        }
    }
}

const MAX_INCOMPLETE_FRAME_ALLOWED: u8 = 3;

#[derive(Default)]
pub struct MessageHandler {
    buffer: String,
    messages: Vec<MessageData>,
    map: HashMap<u8, Vec<FragmentRawData>>,
    incomplete_frame: u8,
}

impl MessageHandler {
    pub fn new() -> MessageHandler {
        Self::default()
    }

    pub fn handler(&mut self, input: &str) -> &Vec<MessageData> {
        self.messages = vec![];
        let mut packet = input;
        loop {
            if !self.handle_block(packet) {
                break;
            }
            packet = "";
        }
        &self.messages
    }

    fn on_success(&mut self, fragment: FragmentRawData) {
        self.incomplete_frame = 0;

        if fragment.header.number_of_fragment == 1 {
            self.messages.push(MessageData::new(fragment));
            return;
        }

        if fragment.header.message_id.is_none() {
            return;
        }

        let message_id = fragment.header.message_id.unwrap();

        self.map
            .entry(message_id)
            .or_insert_with(std::vec::Vec::new);

        let number_of_fragment = fragment.header.number_of_fragment;

        if number_of_fragment == 0 {
            return;
        }

        if number_of_fragment != self.map.get(&message_id).unwrap().len() as u8 {
            self.map.get_mut(&message_id).unwrap().push(fragment);

            if number_of_fragment == self.map.get(&message_id).unwrap().len() as u8 {
                self.map.get_mut(&message_id).unwrap().sort();

                let mut fragments = self.map.remove(&message_id).unwrap().into_iter();

                let first = fragments.next().unwrap();

                let message: MessageData =
                    fragments.fold(first, |acc, current| acc + current).into();

                self.messages.push(message);
            }
        }
    }

    fn handle_block(&mut self, block: &str) -> bool {
        let input = format!("{}{}", self.buffer, block);

        // Parse the given fragment
        // If the fragment is incomplete buffers the partial fragment
        // If invalid rejects it
        let result_parse = parse_fragment(&input[..]);

        match result_parse {
            Err(nom::Err::Incomplete(_needed)) => {
                self.incomplete_frame += 1;

                if self.incomplete_frame > MAX_INCOMPLETE_FRAME_ALLOWED {
                    self.buffer = "".to_string();
                    return false;
                }

                self.buffer = input;
                false
            }
            Err(nom::Err::Error(err)) | Err(nom::Err::Failure(err)) => {
                self.buffer = err.input.to_string();
                if !self.buffer.is_empty() {
                    return true;
                }
                false
            }
            Ok((remain, (header, compressed_payload, pad, checksum_value, message_to_check))) => {
                // Verifies the fragment checksum
                let result_checksum = checksum(message_to_check, checksum_value);
                if !result_checksum {
                    self.buffer = remain.to_string();
                    return false;
                }

                // Uncompress the payload
                // If the payload can't be parsed reject the fragment
                let result_uncompress = uncompress_payload(compressed_payload);

                if result_uncompress.is_err() {
                    self.buffer = remain.to_string();
                    return false;
                }

                let payload = result_uncompress.unwrap();
                // ignore padding bits for
                let payload = payload[0..payload.len() - pad as usize].to_string();

                let fragment = FragmentRawData { header, payload };
                self.buffer = remain.to_string();
                self.on_success(fragment);
                true
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::parsers::header::{Channel, Header, Protocol, ProtocolType, TalkerId};
    use crate::parsers::utils::stream::{
        FragmentRawData, MessageData, MessageHandler, MessageHeader, MAX_INCOMPLETE_FRAME_ALLOWED,
    };

    #[test]
    fn should_return_a_complete_fragment() {
        let message = "!AIVDM,1,1,,B,B43JRq00LhTWc5VejDI>wwWUoP06,0*29\r\n";
        let message_data = MessageData {
            header: MessageHeader {
                protocol: Protocol {
                    talker_id: TalkerId::AI,
                    protocol_type: ProtocolType::VDM
                },
                channel: Channel::B
            },
            payload: "010010000100000011011010100010111001000000000000011100110000100100100111101011000101100110101101110010010100011001001110111111111111100111100101110111100000000000000110".to_string()
        };

        let mut message_handler = MessageHandler::new();
        let result = message_handler.handler(message);

        assert_eq!(result, &vec![message_data]);
    }

    #[test]
    fn should_reject_fragmented_message_with_message_id_equal_none() {
        let part1 = "!AIVDM,2,1,,B,59NS6oP2=lu07PD";
        let part2 = "WJ21Tn1=D<<E=>22222222217FiLN:4o:0PTj1Bkm,0*78\r\n";
        let part3 = "!AIVDM,2,2,5,B,H88888888888880,2*52\r\n";
        let frame = format!("{}{}{}", part1, part2, part3);

        let mut message_handler = MessageHandler::new();
        let result = message_handler.handler(&frame);

        assert_eq!(result, &vec![]);
    }

    #[test]
    fn should_reject_fragmented_message_with_number_of_fragment_equals_0() {
        let part1 = "!AIVDM,0,1,5,B,59NS6oP2=lu07PD";
        let part2 = "WJ21Tn1=D<<E=>22222222217FiLN:4o:0PTj1Bkm,0*4F\r\n";
        let part3 = "!AIVDM,2,2,5,B,H88888888888880,2*52\r\n";
        let frame = format!("{}{}{}", part1, part2, part3);

        let mut message_handler = MessageHandler::new();
        let result = message_handler.handler(&frame);

        assert_eq!(result, &vec![]);
    }

    #[test]
    fn should_eat_all_the_frame() {
        let frame = ",,,,,,1.3,3.8,3.1*05\r\n$GPZDA,062254.650,24,11,2020,01,00*55\r\n!AIVDO,1,1,,A,17PaewhP1mwae3PJs1fpW6qd0000,0*1F\r\n,A,3,8,11,15,22,,";

        let message_data = MessageData {
            header: MessageHeader {
                protocol: Protocol {
                    talker_id: TalkerId::AI,
                    protocol_type: ProtocolType::VDO
                },
                channel: Channel::A
            },
            payload: "000001000111100000101001101101111111110000100000000001110101111111101001101101000011100000011010111011000001101110111000100111000110111001101100000000000000000000000000".to_string()
        };

        let mut message_handler = MessageHandler::new();
        let result = message_handler.handler(frame);

        assert_eq!(result, &vec![message_data]);
    }

    #[test]
    fn should_break_at_error_and_no_more_feed() {
        let frame = "!AIVDO,1,1,,C,17PaewhP1mwae3PJs1fpW6qd0000,0*1F\r\n";

        let mut message_handler = MessageHandler::new();
        let result = message_handler.handler(frame);

        assert_eq!(result, &vec![]);
    }

    #[test]
    fn should_buffer_incomplete_fragment() {
        let part1 = "!AIVDM,2,1,5,B,59NS6oP2=lu07PD";
        let part2 = "WJ21Tn1=D<<E=>22222222217FiLN:4o:0PTj1Bkm,0*4D\r\n";
        let part3 = "!AIVDM,2,2,5,B,H88888888888880,2*52\r\n";
        let part4 = "!AIVDM,1,1,,A,15N7th0P00ISsi4A5I?:fgvP2<40,0*06\r\n";

        let message_data1 = MessageData {
            header: MessageHeader {
                protocol: Protocol {
                    talker_id: TalkerId::AI,
                    protocol_type: ProtocolType::VDM
                },
                channel: Channel::B
            },
            payload: "0001010010010111101000110001101101111000000000100011011101001111010000000001111000000101001001110110100000100000011001001101100000010011010101000011000011000101010011010011100000100000100000100000100000100000100000100000100000100000010001110101101100010111000111100010100001001101110010100000001000001001001100100000010100101100111101010110000010000010000010000010000010000010000010000010000010000010000010000010000010000000".to_string()
        };

        let message_data2 = MessageData {
            header: MessageHeader {
                protocol: Protocol {
                    talker_id: TalkerId::AI,
                    protocol_type: ProtocolType::VDM
                },
                channel: Channel::A
            },
            payload: "000001000101011110000111111100110000000000100000000000000000011001100011111011110001000100010001000101011001001111001010101110101111111110100000000010001100000100000000".to_string()
        };

        let mut message_handler = MessageHandler::new();

        let result1 = message_handler.handler(part1);
        assert_eq!(result1, &vec![]);

        let result2 = message_handler.handler(part2);
        assert_eq!(result2, &vec![]);

        let result3 = message_handler.handler(part3);
        assert_eq!(result3, &vec![message_data1]);

        let result4 = message_handler.handler(part4);
        assert_eq!(result4, &vec![message_data2]);
    }

    #[test]
    fn should_reject_message_with_checksum_error() {
        // checksum not match with payload
        let part = "!AIVDM,1,1,,A,15N7th0P00ISsi4A5I?:fgvP2<40,0*07";

        let mut message_handler = MessageHandler::new();
        assert_eq!(message_handler.handler(part), &vec![]);

        // payload not match with checksum value
        let part = "!AIVDM,1,1,,A,15N42th0P00ISsi4A5I?:fgvP2<40,0*06";

        let mut message_handler = MessageHandler::new();
        assert_eq!(message_handler.handler(part), &vec![]);
    }

    #[test]
    fn should_reject_message_with_checksum_error_fragmented() {
        let part1 = "!AIVDM,1,1,,A,15N7th0P00";
        let part2 = "ISsi4A5I?:fgvP2<40,0*07\r\n!AIVDM,1,1,,A,15N7th0P0";
        // same message but with the correct checksum
        let part3 = "0ISsi4A5I?:fgvP2<40,0*06\r\n";

        let mut message_handler = MessageHandler::new();
        assert_eq!(message_handler.handler(part1), &vec![]);
        assert_eq!(message_handler.handler(part2), &vec![]);

        let message = MessageData {
            header: MessageHeader {
                protocol: Protocol {
                    talker_id: TalkerId::AI,
                    protocol_type: ProtocolType::VDM
                },
                channel: Channel::A
            },
            payload: "000001000101011110000111111100110000000000100000000000000000011001100011111011110001000100010001000101011001001111001010101110101111111110100000000010001100000100000000".to_string(),
        };

        assert_eq!(message_handler.handler(part3), &vec![message]);
    }

    #[test]
    fn should_reject_invalid_message() {
        let part1 = "!AIVDM,2,2,4";
        let part2 = ",Z,00000000000,2*23\r\n!AIVDM,1,1,,A,15N7";

        let part3 = "th0P00ISsi4A5I?:fgvP2<40,0*06\r\n";

        let message = MessageData {
            header: MessageHeader {
                protocol: Protocol {
                    talker_id: TalkerId::AI,
                    protocol_type: ProtocolType::VDM
                },
                channel: Channel::A
            },
            payload: "000001000101011110000111111100110000000000100000000000000000011001100011111011110001000100010001000101011001001111001010101110101111111110100000000010001100000100000000".to_string(),
        };

        let mut message_handler = MessageHandler::new();
        assert_eq!(message_handler.handler(part1), &vec![]);
        assert_eq!(message_handler.handler(part2), &vec![]);

        assert_eq!(message_handler.handler(part3), &vec![message]);
    }

    #[test]
    fn should_flush_frame_buffer_when_uncompleted_allowed_frames_exceed() {
        let message = "!AIVDM,1,1,,B,B43JRq00LhTWc5VejDI>wwWUoP06,0*29\r\n";
        let message_data = MessageData {
            header: MessageHeader {
                protocol: Protocol {
                    talker_id: TalkerId::AI,
                    protocol_type: ProtocolType::VDM
                },
                channel: Channel::B
            },
            payload: "010010000100000011011010100010111001000000000000011100110000100100100111101011000101100110101101110010010100011001001110111111111111100111100101110111100000000000000110".to_string()
        };

        let mut message_handler = MessageHandler::new();

        for _i in 0..=MAX_INCOMPLETE_FRAME_ALLOWED {
            let incomplete = "!AIVDM,2,2,4";
            assert_eq!(message_handler.handler(incomplete), &vec![]);
        }

        assert_eq!(message_handler.handler(message), &vec![message_data]);
    }

    #[test]
    fn should_reject_message_that_cant_be_uncompress() {
        let part1 = "!AIVDM,2,1,5,B,59NS6oPz=lu07PD";
        let part2 = "WJ21Tn1=D<<E=>22222222217FiLN:4o:0PTj1Bkm,0*05\r\n!AIVDM,1,1,,A,15N7";
        let part3 = "th0P00ISsi4A5I?:fgvP2<40,0*06\r\n";

        let message = MessageData {
            header: MessageHeader {
                protocol: Protocol {
                    talker_id: TalkerId::AI,
                    protocol_type: ProtocolType::VDM
                },
                channel: Channel::A
            },
            payload: "000001000101011110000111111100110000000000100000000000000000011001100011111011110001000100010001000101011001001111001010101110101111111110100000000010001100000100000000".to_string(),
        };

        let mut message_handler = MessageHandler::new();

        assert_eq!(message_handler.handler(part1), &vec![]);
        assert_eq!(message_handler.handler(part2), &vec![]);
        assert_eq!(message_handler.handler(part3), &vec![message]);
    }

    #[test]
    fn should_handle_padded_payload() {
        let part =
            "!AIVDM,1,1,5,B,59NS6oP2=lu07PDWJ21Tn1=D<<E=>22222222217FiLN:4o:0PTj1Bkm,5*4B\r\n";

        let message = MessageData {
            header: MessageHeader {
                protocol: Protocol {
                    talker_id: TalkerId::AI,
                    protocol_type: ProtocolType::VDM
                },
                channel: Channel::B
            },
            payload: "0001010010010111101000110001101101111000000000100011011101001111010000000001111000000101001001110110100000100000011001001101100000010011010101000011000011000101010011010011100000100000100000100000100000100000100000100000100000100000010001110101101100010111000111100010100001001101110010100000001000001001001100100000010100101100111".to_string(),
        };

        let mut message_handler = MessageHandler::new();

        assert_eq!(message_handler.handler(part), &vec![message]);
    }

    #[test]
    fn should_accept_fragmented_messages() {
        let payload = "!AIVDM,1,1,,B,53nFBv01SJ<thHp6220H4heHTf2222222222221?50:454o<`9QSlUDp,0*33\r\n!AIVDM,1,1,,B,888888888888880,2*17\r\n";

        let mut message_handler = MessageHandler::new();
        message_handler.handler(payload);

        let message1 = MessageData {
            header: MessageHeader {
                protocol: Protocol {
                    talker_id: TalkerId::AI,
                    protocol_type: ProtocolType::VDM
                },
                channel: Channel::B
            },
            payload: "000101000011110110010110010010111110000000000001100011011010001100111100110000011000111000000110000010000010000000011000000100110000101101011000100100101110000010000010000010000010000010000010000010000010000010000010000010000010000001001111000101000000001010000100000101000100110111001100101000001001100001100011110100100101010100111000".to_string()
        };

        let message2 = MessageData {
            header: MessageHeader {
                protocol: Protocol {
                    talker_id: TalkerId::AI,
                    protocol_type: ProtocolType::VDM
                },
                channel: Channel::B
            },
            payload: "0010000010000010000010000010000010000010000010000010000010000010000010000010000010000000".to_string()
        };

        assert_eq!(message_handler.handler(payload), &vec![message1, message2])
    }

    #[test]
    fn should_add_two_fragments() {
        let fragment1 = FragmentRawData {
            header: Header {
                protocol: Protocol {
                    talker_id: TalkerId::AI,
                    protocol_type: ProtocolType::VDM
                },
                number_of_fragment: 2,
                current_fragment: 1,
                message_id: Some(9),
                channel: Channel::B
            },
            payload: "000101000011110110010110010010111110000000000001100011011010001100111100110000011000111000000110000010000010000000011000000100110000101101011000100100101110000010000010000010000010000010000010000010000010000010000010000010000010000001001111000101000000001010000100000101000100110111001100101000001001100001100011110100100101010100111000".to_string()
        };

        let fragment2 = FragmentRawData {
            header: Header {
                protocol: Protocol {
                    talker_id: TalkerId::AI,
                    protocol_type: ProtocolType::VDM
                },
                number_of_fragment: 2,
                current_fragment: 2,
                message_id: Some(9),
                channel: Channel::B
            },
            payload: "001000001000001000001000001000001000001000001000001000001000001000001000001000001000000".to_string()
        };

        let final_payload = format!("{}{}", fragment1.payload, fragment2.payload);

        assert_eq!(
            fragment1 + fragment2,
            FragmentRawData {
                header: Header {
                    protocol: Protocol {
                        talker_id: TalkerId::AI,
                        protocol_type: ProtocolType::VDM
                    },
                    message_id: Some(9),
                    current_fragment: 2,
                    number_of_fragment: 2,
                    channel: Channel::B
                },
                payload: final_payload
            }
        )
    }
}
